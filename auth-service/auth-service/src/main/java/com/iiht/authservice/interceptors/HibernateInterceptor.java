package com.iiht.authservice.interceptors;

import java.io.Serializable;
import java.util.Arrays;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@SuppressWarnings("deprecation")
@Component
public class HibernateInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(HibernateInterceptor.class);
	
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		logger.info("BaseModelInterceptor:: onSave ...");
		setValue(state, propertyNames, "createdDate", new java.util.Date());
		return true;
	}

	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] state, Object[] previousState,String[] propertyNames, Type[] types) {
		logger.info("BaseModelInterceptor:: onFlushDirty ...");
		setValue(state, propertyNames, "modifiedDate", new java.util.Date());
		return true;
	}
	
	@Override
	public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		logger.info("BaseModelInterceptor:: onDelete ...");
		setValue(state, propertyNames, "modifiedDate", new java.util.Date());
	}

	private void setValue(Object[] currentState, String[] propertyNames, String propertyToSet, Object value) {
		int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		}
	}
}
