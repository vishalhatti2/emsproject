package com.iiht.authservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.authservice.entity.UserRoleMapper;

@Repository
public interface UserRoleMapperRepository extends CrudRepository<UserRoleMapper, Long> {


	@Query("FROM UserRoleMapper mapper "
			 + "LEFT JOIN FETCH mapper.user AS user "
			 + "LEFT JOIN FETCH mapper.role AS role "
			 + "WHERE  user.email = :email")
	List<UserRoleMapper> getUserRolesByUserRoleMapper(String email);
	
}
