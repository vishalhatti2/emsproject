package com.iiht.authservice.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iiht.authservice.dto.BaseDto;

import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomUserDetailsService jwtUserDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		
		
		String uri = request.getRequestURI();
		
		final String requestTokenHeader = request.getHeader("Authorization");

		
		if (uri.indexOf("authenticate") >= 0 || uri.indexOf("register") >= 0 ) {
			chain.doFilter(request, response); // If not valid, go to the next filter.
			return;
		}
		

		if(requestTokenHeader == null || !requestTokenHeader.startsWith("Bearer ")) {
			response.setStatus(401);
			response.setHeader("Content-Type", "application/json");
            response.getOutputStream().write(new ObjectMapper().writeValueAsString(new BaseDto(null, "You are not authenticated. Access token is required to access this resource.", null, null)).getBytes());
            return;
		}
				
		String token = requestTokenHeader.replace("Bearer ", "");


		
try {	
			
			// 4. Validate the token
			Claims claims =jwtTokenUtil.getAllClaimsFromToken(token);
			
			String username = claims.getSubject();
			
			if(username != null) {
				

				UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);

				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				// After setting the Authentication in the context, we specify
				// that the current user is authenticated. So it passes the Spring Security Configurations successfully.
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
			
		} catch (Exception e) {
			// In case of failure. Make sure it's clear; so guarantee user won't be authenticated
			e.printStackTrace();
			SecurityContextHolder.clearContext();
			
			if(e.getMessage() != null && e.getMessage().indexOf("JWT expired") >= 0) {
				response.setStatus(406);
				response.setHeader("Content-Type", "application/json");
	            response.getOutputStream().write(new ObjectMapper().writeValueAsString(new BaseDto(null, "Token expired ...!!!", null, null)).getBytes());
	            return;
			}
		}
		

		chain.doFilter(request, response);
	}

}
