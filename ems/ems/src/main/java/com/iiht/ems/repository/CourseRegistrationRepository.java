package com.iiht.ems.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.ems.model.Registration;

@Repository
public interface CourseRegistrationRepository extends CrudRepository<Registration, Long>{

}
