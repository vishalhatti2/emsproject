package com.iiht.ems.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "registration")
@EqualsAndHashCode(callSuper = true)
@Data
public class Registration extends BaseEntity{

	private String email;
	private  String course;
}
