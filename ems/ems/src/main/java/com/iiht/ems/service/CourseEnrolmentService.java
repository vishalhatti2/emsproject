package com.iiht.ems.service;

import com.iiht.ems.dto.CourseRegistrationDto;
import com.iiht.ems.model.Registration;

public interface CourseEnrolmentService {

	public Registration enrollCourse (CourseRegistrationDto courseRegistrationDto);
}
