package com.iiht.ems.serviceImpl;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iiht.ems.dto.CourseRegistrationDto;
import com.iiht.ems.service.NotifiationService;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class NotifiationServiceImpl implements NotifiationService{
	

	@Override
	public boolean sendRegistrationNotification(CourseRegistrationDto registraationDto) throws URISyntaxException {
		
		boolean success = false;
		
		 RestTemplate restTemplate = new RestTemplate();
		 
		 URI uri = new URI("http://localhost:" + "9009" + "/notify");

		 ResponseEntity<CourseRegistrationDto> result = restTemplate.postForEntity(uri,registraationDto,CourseRegistrationDto.class);
		
		 if (result.getStatusCode()==HttpStatusCode.valueOf(200)) {
			 
			  success = true;
		 }
		return success;
		
	}

	
}
