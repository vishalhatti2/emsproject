package com.iiht.ems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.ems.dto.CourseRegistrationDto;
import com.iiht.ems.service.CourseEnrolmentService;
import com.iiht.ems.service.NotifiationService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value="/enroll")
@Slf4j
public class CourseRegistrationController {
	
	@Autowired
	private CourseEnrolmentService courseEnrolmentService;
	
	@Autowired
	private NotifiationService notificationServie;
	
	@PostMapping
	public ResponseEntity<CourseRegistrationDto> registerCourse(@RequestBody CourseRegistrationDto courseRegistrationDto){
		
		CourseRegistrationDto response = new CourseRegistrationDto ();

		try {
			
			courseEnrolmentService.enrollCourse(courseRegistrationDto);
			
			notificationServie.sendRegistrationNotification(courseRegistrationDto);
			
			response.setMessage("Success");
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Unable to register course",e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}

}
