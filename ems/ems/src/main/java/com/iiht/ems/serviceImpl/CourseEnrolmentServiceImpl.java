package com.iiht.ems.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.ems.dto.CourseRegistrationDto;
import com.iiht.ems.model.Registration;
import com.iiht.ems.repository.CourseRegistrationRepository;
import com.iiht.ems.service.CourseEnrolmentService;

@Service
public class CourseEnrolmentServiceImpl implements CourseEnrolmentService{
	
	@Autowired
	private CourseRegistrationRepository courseRegistrationRepository;

	
	public Registration enrollCourse(CourseRegistrationDto courseRegistrationDto) {
		Registration registration = new Registration();
		registration.setCourse(courseRegistrationDto.getCourse());
		registration.setEmail(courseRegistrationDto.getEmail());
		return courseRegistrationRepository.save(registration);
	}

}
