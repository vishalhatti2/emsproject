package com.iiht.registrationservice.serviceImpl;

import com.iiht.registrationservice.dto.CourseDto;
import com.iiht.registrationservice.exception.CourseAlreadyExistException;
import com.iiht.registrationservice.model.Course;
import com.iiht.registrationservice.repository.CourseRepository;
import com.iiht.registrationservice.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourrseServiceImpl implements CourseService{

	@Autowired
	 private  CourseRepository courseRepository;
	
	@Override
	public Course addCourse(CourseDto courseDto) {
		
		Optional<Course> courseOptional = courseRepository.findByName(courseDto.getName().toLowerCase());
		if (courseOptional.isPresent()) {
			throw new CourseAlreadyExistException("Course already Exist Exception");
		}
		else {
			Course course = new Course(courseDto.getName().toLowerCase(),courseDto.getCode().toLowerCase());
			
			return courseRepository.save(course);
			
		}
		
	}

	@Override
	public void deleteCourse(Long couseId) {
		
		Optional<Course> courseOptional = courseRepository.findById(couseId);
		if (courseOptional.isPresent()) {
		courseRepository.delete(courseOptional.get());
		}
		
	}

	

}
