package com.iiht.registrationservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)

public class BaseDto {

	private Long id;
	private String errorMsg;
	private String message;
	private String statusCode;
	
	public BaseDto() {}
	
	public BaseDto(Long id) {
		this.id = id;
	}

}
