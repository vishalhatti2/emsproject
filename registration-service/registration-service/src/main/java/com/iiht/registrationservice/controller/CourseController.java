package com.iiht.registrationservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.registrationservice.dto.CourseDto;
import com.iiht.registrationservice.dtoMapper.CourseDtoMapper;
import com.iiht.registrationservice.model.Course;
import com.iiht.registrationservice.service.CourseService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value="/course")
@Slf4j
@AllArgsConstructor
public class CourseController {
	
	
	 private final  CourseService courseService;

	@PostMapping
	public ResponseEntity<CourseDto> addFaculity(@RequestBody CourseDto courseDto ){
		
		CourseDto response = new CourseDto();

			Course course = courseService.addCourse(courseDto);
			response = CourseDtoMapper.toCourseDto(course);
			return new ResponseEntity<>(response, HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("Unable to add course",e.getMessage());
//			response.setErrorMsg(e.getMessage());
//			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

	}
	
	@GetMapping
	public String addFaculity(){
		
		return "hello";
	}	

}
