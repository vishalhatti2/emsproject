package com.iiht.registrationservice.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CourseExceptionHandler {
    @ExceptionHandler({CourseAlreadyExistException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<?> invalid(CourseAlreadyExistException ex, HttpServletRequest request){
        Map<String,String> errors = new HashMap<>();
        errors.put("errors",ex.getLocalizedMessage());
        return new ResponseEntity<>(errors,HttpStatus.OK);

    }
}
