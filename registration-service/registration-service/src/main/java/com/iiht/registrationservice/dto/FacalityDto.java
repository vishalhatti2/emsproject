package com.iiht.registrationservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FacalityDto extends BaseDto{

	private String name;
	private String email;
	private String courseName;
	private List<FacalityDto> faculityList;
}
