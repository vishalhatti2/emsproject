package com.iiht.registrationservice.exception;

public class CourseAlreadyExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public CourseAlreadyExistException(String courseAlreadyExistException) {
		super(courseAlreadyExistException,new Throwable());
	}
}
