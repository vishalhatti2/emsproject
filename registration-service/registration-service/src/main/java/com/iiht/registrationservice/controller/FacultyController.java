package com.iiht.registrationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.registrationservice.dto.FacalityDto;
import com.iiht.registrationservice.dtoMapper.FaculityDtoMapper;
import com.iiht.registrationservice.model.Facuility;
import com.iiht.registrationservice.service.FaculityService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value="/facality")
@Slf4j
	public class FacultyController {

	@Autowired
	private  FaculityService faculityService;
	
	@PostMapping
	public ResponseEntity<FacalityDto> addFaculity(@RequestBody FacalityDto facalityDto ){
		log.info("inside add faculity");
		FacalityDto response = new FacalityDto();
		try {
			Facuility faculity = faculityService.addFaculty(facalityDto);
			response = FaculityDtoMapper.toFaculityDto(faculity);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Unable to add faculity",e.getMessage());
			response.setErrorMsg(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
}
