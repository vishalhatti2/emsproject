package com.iiht.registrationservice.serviceImpl;

import com.iiht.registrationservice.dto.FacalityDto;
import com.iiht.registrationservice.exception.CourseAlreadyExistException;
import com.iiht.registrationservice.model.Course;
import com.iiht.registrationservice.model.Facuility;
import com.iiht.registrationservice.repository.CourseRepository;
import com.iiht.registrationservice.repository.FaculityRepository;
import com.iiht.registrationservice.service.FaculityService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class FaculityServiceImpl implements FaculityService {
	
	private final FaculityRepository faculityRepository;
	private final CourseRepository courseRepository;

	@Override
	public Facuility addFaculty(FacalityDto faculitydto) {
		final Facuility facuility = new Facuility();
		facuility.setName(faculitydto.getName());
		facuility.setEmail(faculitydto.getEmail());
		Optional<Course> optionalCourse = courseRepository.findByName(faculitydto.getCourseName());
		if (optionalCourse.isPresent()) {
			facuility.setCourse(optionalCourse.get());
			return faculityRepository.save(facuility);
		}
		else {
			throw new CourseAlreadyExistException("Course Already exist exception");

		}
	}

	@Override
	public List<Facuility> getListByCourseId(Long courseId) {
		// TODO Auto-generated method stub
		return null;
	}

}
