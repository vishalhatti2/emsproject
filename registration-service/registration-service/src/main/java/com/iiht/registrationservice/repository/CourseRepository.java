package com.iiht.registrationservice.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iiht.registrationservice.model.Course;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long>{

	Optional<Course>  findByName(String name);

}
