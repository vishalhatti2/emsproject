package com.iiht.registrationservice.service;

import java.util.List;

import com.iiht.registrationservice.dto.FacalityDto;
import com.iiht.registrationservice.model.Facuility;

public interface FaculityService {

	public Facuility addFaculty(FacalityDto facalityDto);
	public List<Facuility> getListByCourseId(Long courseId);
	


}
