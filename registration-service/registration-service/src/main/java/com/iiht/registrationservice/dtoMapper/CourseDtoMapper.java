package com.iiht.registrationservice.dtoMapper;

import com.iiht.registrationservice.dto.CourseDto;
import com.iiht.registrationservice.model.Course;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CourseDtoMapper {

	public static Course toCourse (CourseDto courseDto) {
		
		Course course = new Course();
		course.setCode(courseDto.getCode());
		course.setName(courseDto.getName());
		return course;
	}
	
public static CourseDto toCourseDto (Course course) {
	
		CourseDto dto = new CourseDto();
		dto.setCode(course.getCode());
		dto.setName(course.getName());
		return dto;
	}
}
