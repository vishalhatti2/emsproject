package com.iiht.notificationservice.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.iiht.notificationservice.dto.CourseRegistrationDto;
import com.iiht.notificationservice.service.MailService;

import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MailServiceImpl implements MailService{
	
	@Autowired
	public JavaMailSender mailSender;


	@Override
	public void sendMultipleMailsthroughHtml( String text)  {
		
		
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED,
					"utf-8");
			
			CourseRegistrationDto dto = new Gson().fromJson(text, CourseRegistrationDto.class);
			String htmlMessage = "<html>"
					+ "<table style=' border: 1px solid black'>"
					+ "<tr>"
					+ "<th style=' border: 1px solid black'>Name</th>"
					+ "<th style=' border: 1px solid black'>Course</th>"
					+ "</tr>"
					+ "<tr>"
					+ "<td style=' border: 1px solid black'>"+dto.getName() + "</td>"
					+ "<td style=' border: 1px solid black'>"+dto.getCourse() +"</td>"
					+ "</tr>"
					+ "</table>"
					+ "</html>";
			helper.setText(htmlMessage, true);
			helper.setFrom("vishalhatti2@gmail.com");
			helper.setTo("geetashivraj96@gmail.com");
			helper.setSubject("Thanks for registering the course");
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new RuntimeException("Error in sending email");
		}
	}
	


}
